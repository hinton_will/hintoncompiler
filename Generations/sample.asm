#Generated from Mini-Pascal using code
#written by William Hinton
.data
dollars:		.word	0
yen:		.word	0
bitcoins:		.word	0


.text
main:
addi	$s0,	$zero,	1000000
sw	$s0	dollars

lw	$t0,	dollars
addi	$t1,	$zero,	114
mult	$t0,	$t1
mflo	$s1
sw	$s1	yen

lw	$t0,	dollars
addi	$t1,	$zero,	1184
div	$t0,	$t1
mflo	$s2
sw	$s2	bitcoins

addi	$v0,	10
li	$v0,	10
syscall
