This program compiles a subset of Mini-Pascal into MIPS assembly. It doesn't support arrays, floats, or functions at this time. To run the program from the command line, enter the command

java -jar compiler.jar [filepath]

The generated code will appear in the Generations folder.