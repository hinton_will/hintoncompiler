package SymbolTable;

import java.util.HashMap;
import java.util.Set;

public class SymbolTable{
    
    private HashMap table = new HashMap(); 
    
    public void showKeys(){
        Set set = table.entrySet();
        System.out.println(set);
    }
    
    public boolean isProcedureName(String key){
        Object x = table.get(key);
        if(x == SymbolType.PROCEDURE_NAME){
            return true;
        }
        else{
        return false;
        }
    }
    
    public boolean isVariableName(String key){
        Object x = table.get(key);
        if(x == SymbolType.VARIABLE_NAME){
            return true;
        }
        else{
        return false;
        }
    }
    
    public boolean isProgramName(String key){
        Object x = table.get(key);
        if(x == SymbolType.PROGRAM_NAME){
            return true;
        }
        else{
        return false;
        }
    }
    
    public boolean isFunctionName(String key){
        Object x = table.get(key);
        if(x == SymbolType.FUNCTION_NAME){
            return true;
        }
        else{
        return false;
        }
    }
    
    
    
    public void addVariableName(String name){
        table.put(name, SymbolType.VARIABLE_NAME);
    }    
    
    public void addProgramName(String name){
        table.put(name, SymbolType.PROGRAM_NAME);
    }    
    
    public void addFunctionName(String name){
        table.put(name, SymbolType.FUNCTION_NAME);
    }
    
    public void addProcedureName(String name){
        table.put(name, SymbolType.PROCEDURE_NAME);
    }
}
