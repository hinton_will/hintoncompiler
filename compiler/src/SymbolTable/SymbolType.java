/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SymbolTable;

/**
 *
 * @author Will
 */
public enum SymbolType {
    VARIABLE_NAME,
    PROCEDURE_NAME,
    PROGRAM_NAME,
    FUNCTION_NAME
}
