package codeGeneration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import scanner.TokenType;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.CompoundStatementNode;
import syntaxtree.DeclarationsNode;
import syntaxtree.ExpressionNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;

/**
 * This class will create code for an Equation tree.
 * @author Erik Steinmetz
 */
public class CodeGeneration {
    private int currentTRegister = 0;
    private int currentSRegister = 0;
    
    public CodeGeneration(ProgramNode tree, String treeName) throws IOException{
        String code = writeCodeForRoot(tree);
        System.out.println(code);
        
        File generatedCode = new File("../../Generations/" + treeName +".asm");
        BufferedWriter out = new BufferedWriter(new FileWriter(generatedCode)); 
        out.write(code);
        out.close();
    }
    
    /**
     * Starts the code from the root node by writing the outline of the
     * assembly code, and telling the root node to write its answer into $s0.
     *
     * @param root The root node of the equation to be written
     * @return A String of the assembly code.
     */
    public String writeCodeForRoot( ProgramNode root) 
    //public String writeCodeForRoot( ExpressionNode root) 
    {
        DeclarationsNode declarations = root.getVariables();
        CompoundStatementNode main = root.getMain();
        
        String nodeCode = null;
        
        StringBuilder code = new StringBuilder();
        code.append( "#Generated from Mini-Pascal using code\n");
        code.append( "#written by William Hinton\n");
        
        //declarations
        code.append( ".data\n");
        nodeCode = writeDeclarations(declarations, "$s0");
        //nodeCode = writeCode(root, "$s0");
        code.append(nodeCode);
        //clear out nodeCode to avoid reprinting declarations
        nodeCode = null;
        //code.append( "answer:   .word   0\n\n\n");
        
        //compound statements
        code.append( "\n\n.text\n");
        code.append( "main:\n");
        
        int tempTRegValue = this.currentTRegister;
        nodeCode = writeCode( main, "$s0");
        
        
        this.currentTRegister = tempTRegValue;
        code.append( nodeCode);
        //code.append( "sw     $s0,   answer\n");
        code.append( "addi\t$v0,\t10\n");
        code.append( "li\t$v0,\t10\n");
        code.append( "syscall\n");
        return( code.toString());
    }
    
    /*Appends declarations for every variable name defined at the beginning of the program. */
    public String writeDeclarations( DeclarationsNode node, String reg){
        String nodeCode = "";
        ArrayList<String> variableNames = node.getVariableNames();
        
        for(String variableName : variableNames){
            nodeCode += (variableName + ":\t\t.word\t0\n");
        }
        
        return nodeCode;
    }
    
    public String writeCode( StatementNode node, String reg){
        String nodeCode = "";
        String sReg = "$s" + this.currentSRegister;
        this.currentSRegister++;
        if(node instanceof AssignmentStatementNode){
            
            nodeCode = writeCode(((AssignmentStatementNode)node).getExpression(), sReg);
            nodeCode += ("sw\t" + sReg + "\t" + ((AssignmentStatementNode) node).getLvalue() +"\n");
            }
        
        return nodeCode;
    }
    /*appends statements to the code*/
    public String writeCode( CompoundStatementNode node, String reg) {
        ArrayList<StatementNode> statements = node.getStatements();
        String nodeCode = "";
        
        for(StatementNode statement : statements){
            nodeCode += writeCode(statement, reg); 
            nodeCode += "\n";
        }
        
        return nodeCode;
    }
    /**
     * Writes code for the given node.
     * This generic write code takes any ExpressionNode, and then
     * recasts according to subclass type for dispatching.
     * @param node The node for which to write code.
     * @param reg The register in which to put the result.
     * @return 
     */
    public String writeCode( ExpressionNode node, String reg) {
        String nodeCode = "";
        if( node instanceof OperationNode) {
            nodeCode = writeCode( (OperationNode)node, reg);
        }
        else if( node instanceof ValueNode) {
            nodeCode = writeCode( (ValueNode)node, reg);
        }
        else if( node instanceof VariableNode) {
            nodeCode = writeCode( (VariableNode)node, reg);
        }
        return( nodeCode);
    }
    
    
    public String writeCode( VariableNode node, String reg) {
        String nodeCode = "";
        nodeCode += ("lw\t" + reg + ",\t" + node.getName() + "\n");
        return( nodeCode);
    }
    /**
     * Writes code for an operations node.
     * The code is written by gathering the child nodes' answers into
     * a pair of registers, and then executing the op on those registers,
     * placing the result in the given result register.
     * @param opNode The operation node to perform.
     * @param resultRegister The register in which to put the result.
     * @return The code which executes this operation.
     */
    public String writeCode( OperationNode opNode, String resultRegister)
    {
        String code;
        ExpressionNode left = opNode.getLeft();
        String leftRegister = "$t" + currentTRegister++;
        code = writeCode( left, leftRegister);
        ExpressionNode right = opNode.getRight();
        String rightRegister = "$t" + currentTRegister++;
        code += writeCode( right, rightRegister);
        TokenType kindOfOp = opNode.getOperation();
        if( kindOfOp == TokenType.PLUS)
        {
            // add resultregister, left, right 
            code += "add\t" + resultRegister + ",\t" + leftRegister +
                    ",\t" + rightRegister + "\n";
        }
        if( kindOfOp == TokenType.MINUS)
        {
            // add resultregister, left, right 
            code += "sub\t" + resultRegister + ",\t" + leftRegister +
                    ",\t" + rightRegister + "\n";
        }
        if( kindOfOp == TokenType.MULTIPLY)
        {
            code += "mult\t" + leftRegister + ",\t" + rightRegister + "\n";
            code += "mflo\t" + resultRegister + "\n";
        }
        
        if( kindOfOp == TokenType.DIVIDE)
        {
            code += "div\t" + leftRegister + ",\t" + rightRegister + "\n";
            code += "mflo\t" + resultRegister + "\n";
        }
        this.currentTRegister -= 2;
        return( code);
    }
    
    /**
     * Writes code for a value node.
     * The code is written by executing an add immediate with the value
     * into the destination register.
     * Writes code that looks like  addi $reg, $zero, value
     * @param valNode The node containing the value.
     * @param resultRegister The register in which to put the value.
     * @return The code which executes this value node.
     */
    public String writeCode( ValueNode valNode, String resultRegister)
    {
        String value = valNode.getAttribute();
        String code = "addi\t" + resultRegister + ",\t$zero,\t" + value + "\n";
        return( code);
    }
}
