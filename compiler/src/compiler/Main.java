/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;
import java.io.IOException;
import parser.Parser;
import codeGeneration.CodeGeneration;
import syntaxtree.ExpressionNode;
import syntaxtree.ProgramNode;
/**
 *
 * @author Will
 */
public class Main {
    

    
    public static void main(String[] args) throws IOException{
        if(args.length < 1){
            System.out.println("No file.");
            System.exit(1);
        }
        
        String filename = args[0];

        Parser instance = new Parser(filename, true);
        ProgramNode tree = instance.program();
        //ExpressionNode tree = instance.expression();
        CodeGeneration cg = new CodeGeneration(tree, tree.getName());
        //instance.printKeys();
    }
    
}
