package parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import scanner.Scanner;
import scanner.Token;
import scanner.TokenType;
import SymbolTable.SymbolTable;
import SymbolTable.SymbolType;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import syntaxtree.ArrayVariableNode;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.CompoundStatementNode;
import syntaxtree.DeclarationsNode;
import syntaxtree.ExpressionNode;
import syntaxtree.IfStatementNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.SubProgramDeclarationsNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import syntaxtree.WhileStatementNode;

/**
 * The parser recognizes whether an input string of tokens
 * is an expression.
 * To use a parser, create an instance pointing at a file,
 * and then call the top-level function, <code>expression()</code>.
 * If the functions returns without an error, the file
 * contains an acceptable expression.
 * @author Erik Steinmetz
 */
public class Parser {
    
    ///////////////////////////////
    //    Instance Variables
    ///////////////////////////////
    
    private Token lookahead;
    private SymbolTable table = new SymbolTable();
    private Scanner scanner;
    
    ///////////////////////////////
    //       Constructors
    ///////////////////////////////
    
    public Parser( String text, boolean isFilename) {
        if( isFilename) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(text);
        } catch (FileNotFoundException ex) {
            error( "No file");
        }
        InputStreamReader isr = new InputStreamReader( fis);
        scanner = new Scanner( isr);
                 
        }
        else {
            scanner = new Scanner( new StringReader( text));
        }
        try {
            lookahead = scanner.nextToken();
        } catch (IOException ex) {
            error( "Scan error");
        }

    }
    
    ///////////////////////////////
    //       Methods
    ///////////////////////////////
    
    public ProgramNode program() throws IOException{
        match(TokenType.PROGRAM);
        
        
        String programName = lookahead.getLexeme();
        table.addProgramName(programName);
        ProgramNode pn = new ProgramNode(programName);
        match(TokenType.ID);
        match(TokenType.SEMI);
        
        
        pn.setVariables( declarations());
        pn.setFunctions(subprogram_declarations());
        pn.setMain(compound_statement());
        match(TokenType.DOT);
        
        saveIndentedToString(programName + ".txt", pn);
        return pn;
    }
    
    public DeclarationsNode declarations(){
        DeclarationsNode dn = null;
        if(lookahead.getType() == TokenType.VAR){
            dn = new DeclarationsNode();
            match(TokenType.VAR);
            table.addVariableName(lookahead.getLexeme());
            ArrayList<String> names = identifier_list();
            match(TokenType.COLON);
            type();
            match(TokenType.SEMI);
            for(String name : names){
                VariableNode vn = new VariableNode(name);
                dn.addVariable(vn);
            }
            declarations();
        }
        else{
            //lambda statement
        }
        return dn;
    }
    
    public SubProgramDeclarationsNode subprogram_declarations(){
        SubProgramDeclarationsNode spdn = null;
        //subprogram_head keywords
        if(lookahead.getType() == TokenType.FUNCTION ||
                lookahead.getType() == TokenType.PROCEDURE){
            subprogram_declaration();
            if(lookahead.getType() == TokenType.SEMI){
                match(TokenType.SEMI);
                subprogram_declarations();
            }
            else{
            error("subprogram declarations");
            }           
        }
        else{
            //lambda statement
        }
        return spdn;
    }
    
    public void subprogram_declaration(){
        subprogram_head();
        declarations();
        subprogram_declarations();
        compound_statement();
    }
    
    public void subprogram_head(){
        if(lookahead.getType() == TokenType.FUNCTION){
            match(TokenType.FUNCTION);
            table.addFunctionName(lookahead.getLexeme());
            match(TokenType.ID);
            arguments();
            match(TokenType.COLON);
            standard_type();
            match(TokenType.SEMI);
        }
        
        else if(lookahead.getType() == TokenType.PROCEDURE){
            match(TokenType.PROCEDURE);
            table.addProcedureName(lookahead.getLexeme());
            match(TokenType.ID);
            arguments();
            match(TokenType.SEMI);
        }
        else{
            error("subprogram declaration");
        }
    }
    
    public ArrayList<VariableNode> arguments(){
        ArrayList<VariableNode> args = new ArrayList<VariableNode>();
        if(lookahead.getType() == TokenType.LEFT_PAREN){
            match(TokenType.LEFT_PAREN);
            args.addAll(parameter_list());
            match(TokenType.RIGHT_PAREN);
        }
        else{
            //lambda statement
        }
        return args;
    }
    
    public ArrayList<VariableNode> parameter_list(){
        ArrayList<VariableNode> params = new ArrayList<VariableNode>();
        ArrayList<String> names = identifier_list();
        if(lookahead.getType() == TokenType.COLON){
            match(TokenType.COLON);
            type();
            for(String name : names){
                VariableNode vn = new VariableNode(name);
                if(table.isVariableName(name)){
                    params.add(vn);
                }
                else{
                    error("Error: Undefined variable found at line " + scanner.getLine() + ", " + lookahead.getLexeme());
                }
            }
            if(lookahead.getType() == TokenType.SEMI){
                match(TokenType.SEMI);
                parameter_list();
            }
            else{
                //lambda statement
            }
        }
        return params;
    }
    
    public ArrayList<String> identifier_list(){
        ArrayList identifiers = new ArrayList<String>();
        if(lookahead.getType() == TokenType.ID){
            table.addVariableName(lookahead.getLexeme());
            identifiers.add(lookahead.getLexeme());
            match(TokenType.ID);
            if(lookahead.getType() == TokenType.COMMA){
                match(TokenType.COMMA);
                identifiers.addAll(identifier_list());
            }
            else{
                //lambda statement
            }
        }
        return identifiers;
    }
    public ExpressionNode expression(){
        ExpressionNode exp = simple_expression();
        if(isRelop(lookahead)){
            OperationNode on = relop();
            on.setLeft(exp);
            on.setRight(simple_expression());
            
            return on;
        }
        else{
            //lambda expression
            return exp;
        }
    }
    
    /**
     * Executes the rule for the simple expression non-terminal symbol in
     * the expression grammar.
     */
    public ExpressionNode simple_expression() {
        //if(lookahead.getType() == TokenType.PLUS || lookahead.getType() == TokenType.MINUS)
        //    sign();
        ExpressionNode aTerm = term();
        return simple_part(aTerm);
    }
    
    /**
     * Executes the rule for the expression&prime; non-terminal symbol in
     * the expression grammar.
     */
    public ExpressionNode simple_part(ExpressionNode possibleLeft) {
        if( lookahead.getType() == TokenType.PLUS || 
                lookahead.getType() == TokenType.MINUS ) {
            OperationNode on = addop();
            on.setLeft(possibleLeft);
            on.setRight(term());
            return simple_part(on);
        }
        else{
            // lambda option
            return possibleLeft;
        }
    }
    
    /**
     * Executes the rule for the addop non-terminal symbol in
     * the expression grammar.
     */
    public OperationNode addop() {
        OperationNode answer = null;
        if( lookahead.getType() == TokenType.PLUS) {
            match( TokenType.PLUS);
            answer = new OperationNode(TokenType.PLUS);
        }
        else if( lookahead.getType() == TokenType.MINUS) {
            match( TokenType.MINUS);
            answer = new OperationNode(TokenType.MINUS);
        }
        else if(lookahead.getType() == TokenType.OR){
            match(TokenType.OR);
            answer = new OperationNode(TokenType.OR);
        }
        else {
            error( "Addop");
        }
        
        return(answer);
    }
    /** The relational operators (relop's) are:
    *   =, <>, <, <=, >=, and >. 
    * */
    public OperationNode relop() {
        OperationNode answer = null;
        
        if( lookahead.getType() == TokenType.EQUAL) {
            match( TokenType.EQUAL);
            answer = new OperationNode(TokenType.EQUAL);
        }
        else if( lookahead.getType() == TokenType.NOT_EQUAL) {
            match( TokenType.NOT_EQUAL);
            answer = new OperationNode(TokenType.NOT_EQUAL);
        }
        else if( lookahead.getType() == TokenType.LESS) {
            match( TokenType.LESS);
            answer = new OperationNode(TokenType.LESS);
        }
        else if( lookahead.getType() == TokenType.GREATER) {
            match( TokenType.GREATER);
            answer = new OperationNode(TokenType.GREATER);
        }
        else if( lookahead.getType() == TokenType.LTE) {
            match( TokenType.LTE);
            answer = new OperationNode(TokenType.LTE);
        }
        else if( lookahead.getType() == TokenType.GTE) {
            match( TokenType.GTE);
            answer = new OperationNode(TokenType.GTE);
        }
        else {
            error( "Relop");
        }
        
        return(answer);
    }
    
    public void assignop() {
        if( lookahead.getType() == TokenType.ASSIGN) {
            match(TokenType.ASSIGN);
        }
        
        else {
            error( "Assignop");
        }
    }
    
    /**
     * Executes the rule for the term non-terminal symbol in
     * the expression grammar.
     */
    public ExpressionNode term() {
        ExpressionNode left = factor();
        return term_part(left);
    }
    
    /**
     * Executes the rule for the term&prime; non-terminal symbol in
     * the expression grammar.
     */
    public ExpressionNode term_part(ExpressionNode possibleLeft) {
        if( isMulop( lookahead) ) {
            OperationNode on = mulop();
            ExpressionNode right = factor();
            on.setRight(right);
            on.setLeft(possibleLeft);

            return term_part(on);
        }
        else{
            return possibleLeft;
            //lambda option
        }
    }
    
    /**
     * Determines whether or not the given token is
     * a mulop token.
     * @param token The token to check.
     * @return true if the token is a mulop, false otherwise
     */
    private boolean isMulop( Token token) {
        boolean answer = false;
        if( token.getType() == TokenType.MULTIPLY || 
                token.getType() == TokenType.DIVIDE ||
                token.getType() == TokenType.DIV ||
                token.getType() == TokenType.MOD ||
                token.getType() == TokenType.AND) {
            answer = true;
        }
        return answer;
    }
    
    private boolean isRelop(Token token){
        boolean answer = false;
        if( token.getType() == TokenType.EQUAL || 
                token.getType() == TokenType.NOT_EQUAL ||
                token.getType() == TokenType.LESS ||
                token.getType() == TokenType.LTE ||
                token.getType() == TokenType.GREATER ||
                token.getType() == TokenType.GTE) {
            answer = true;
        }
        return answer;
    }
    /**
     * Executes the rule for the mulop non-terminal symbol in
     * the expression grammar.
     */
    public OperationNode mulop() {
        OperationNode answer = null;
        if( lookahead.getType() == TokenType.MULTIPLY) {
            match( TokenType.MULTIPLY);
            answer = new OperationNode(TokenType.MULTIPLY);
        }
        else if( lookahead.getType() == TokenType.DIVIDE) {
            match( TokenType.DIVIDE);
            answer = new OperationNode(TokenType.DIVIDE);
        }
        else if(lookahead.getType() == TokenType.DIV){
            match(TokenType.DIV);
            answer = new OperationNode(TokenType.DIV);
        }
        else if(lookahead.getType() == TokenType.MOD){
            match(TokenType.MOD);
            answer = new OperationNode(TokenType.MOD);
        }
        else if(lookahead.getType() == TokenType.AND){
            match(TokenType.AND);
            answer = new OperationNode(TokenType.AND);
        }
        else {
            error( "Mulop");
        }
        
        return(answer);
    }
    
    public ArrayList<ExpressionNode> expression_list(){
        ArrayList<ExpressionNode> expl = new ArrayList<ExpressionNode>();
        expl.add(expression());
        if(isRelop(lookahead)){
            relop();
            expl.addAll(expression_list());
        }
        return expl;
    }
    
    public ArrayList<StatementNode> statement_list(){
        ArrayList<StatementNode> sln = new ArrayList<StatementNode>();
        sln.add(statement());
        if(lookahead.getType() == TokenType.SEMI){
            match(TokenType.SEMI);
            sln.addAll(statement_list());
        }
        else{
            //lambda
        }
        return sln;
    }
    
    public StatementNode statement(){
        StatementNode sn = null;
        switch (lookahead.getType()){
            case ID:
                if(table.isVariableName(lookahead.getLexeme())){
                    AssignmentStatementNode asn = new AssignmentStatementNode();
                    asn.setLvalue(variable());
                    assignop();
                    asn.setExpression(expression());
                    return asn;
                }
                else if(table.isProcedureName(lookahead.getLexeme())){
                    procedure_statement();
                }
                else{
                    error("Undefined variable or procedure name at line " + scanner.getLine() + ": " + lookahead.getLexeme());
                }
                break;
            case BEGIN:
                CompoundStatementNode csn = compound_statement();
                return csn;
            case IF:
                IfStatementNode ifstat = new IfStatementNode();
                match(TokenType.IF);
                ifstat.setTest(expression());
                match(TokenType.THEN);
                ifstat.setThenStatement(statement());
                match(TokenType.ELSE);
                ifstat.setElseStatement(statement());
                return ifstat;
            case WHILE:
                match(TokenType.WHILE);
                WhileStatementNode whilestat = new WhileStatementNode();
                whilestat.setTest(expression());
                match(TokenType.DO);
                whilestat.setDoStatement(statement());
                return whilestat;
            case READ:
                read();
                break;
            case WRITE:
                write();
                break;
            }
          return sn;      
    }
    
    public VariableNode variable(){
        VariableNode vn = new VariableNode(lookahead.getLexeme());
        match(TokenType.ID);
        if(lookahead.getType() == TokenType.LEFT_BRACKET){
            //todo implement array
            ArrayVariableNode avn = new ArrayVariableNode(vn.getName());
            match(TokenType.LEFT_BRACKET);
            expression();
            match(TokenType.RIGHT_BRACKET);
            return avn;
        }
        else{
            //lambda statement
        }
        return vn;
    }
    
    public void procedure_statement(){
        if(table.isProcedureName(lookahead.getLexeme())){
            match(TokenType.ID);
            if(lookahead.getType() == TokenType.LEFT_PAREN){
                match(TokenType.LEFT_PAREN);
                expression_list();
                match(TokenType.RIGHT_PAREN);
            }
            else{
                //lambda statement
            }
        }
    }
    
    public void read(){
        if(lookahead.getType() == TokenType.READ){
            match(TokenType.READ);
            match(TokenType.LEFT_PAREN);
            match(TokenType.ID);
            match(TokenType.RIGHT_PAREN);
        }
        else
            error("read");
    }    
    public void write(){
        if(lookahead.getType() == TokenType.WRITE){
            match(TokenType.WRITE);
            match(TokenType.LEFT_PAREN);
            expression();
            match(TokenType.RIGHT_PAREN);
        }
        else
            error("write");
    }
    
    private boolean isStatement(Token token){
        boolean answer = false;
        if( token.getType() == TokenType.ID || 
                token.getType() == TokenType.BEGIN ||
                token.getType() == TokenType.IF ||
                token.getType() == TokenType.WHILE){
            answer = true;
        }
        return answer;
    }
    
    public CompoundStatementNode compound_statement(){
        CompoundStatementNode csn = null;
        if(lookahead.getType() == TokenType.BEGIN){
            match(TokenType.BEGIN);
            csn = optional_statements();
            match(TokenType.END);
        }
        
        else{
            error("Compound Statement");
        }
        return csn;
    }
    
    public CompoundStatementNode optional_statements(){
        CompoundStatementNode opstatements = new CompoundStatementNode();
        if(isStatement(lookahead)){
            ArrayList<StatementNode> list = statement_list();
            for(StatementNode item : list){
                opstatements.addStatement(item);
            }
        }
        else{
            //lambda
        }
        return opstatements;
    }
    
    public ExpressionNode standard_type(){
        //note: implement types
        ExpressionNode tn = null;
        if(lookahead.getType() == TokenType.INTEGER){
            match(TokenType.INTEGER);
            
        }
        else if(lookahead.getType() == TokenType.REAL){
            match(TokenType.REAL);
        }
        else{
            error("Standard Type");
        }
        return tn;
    }
    
    private boolean isInteger(Token token){
        boolean answer = false;
        if( token.getType() == TokenType.INTEGER || 
                token.getType() == TokenType.REAL){
            answer = true;
        }
        return answer;
    }
    
    //public ExpressionNode type(){
    public void type(){
        if(lookahead.getType() == TokenType.ARRAY){
            match(TokenType.ARRAY);
            match(TokenType.LEFT_BRACKET);
 //           num();
            match(TokenType.NUM);
            match(TokenType.COLON);
 //           num();
            match(TokenType.NUM);
            match(TokenType.RIGHT_BRACKET);
            match(TokenType.OF);            
            ExpressionNode tn = standard_type();
        }
        else if(isInteger(lookahead)){
            ExpressionNode tn = standard_type();
        }
        else{
            error("type");
        }
        //return tn;
    }
    //change
    /*
    public void num(){
        if(lookahead.getType() == TokenType.NUM){
            match(TokenType.NUM);
            optional_fraction();
            optional_exponent();
        }
        else{
            error("num");
        }
    }*/
    
    
    public void sign(){
        if(lookahead.getType() == TokenType.PLUS){
            match(TokenType.PLUS);
        }
        else if(lookahead.getType() == TokenType.MINUS){
            match(TokenType.MINUS);
        }
        else{
            error("sign");
        }
    }
    
    /**
     * Executes the rule for the factor non-terminal symbol in
     * the expression grammar.
     */
    public ExpressionNode factor() {
        // Executed this decision as a switch instead of an
        // if-else chain. Either way is acceptable.
        ExpressionNode answer = null;
        switch (lookahead.getType()) {
            case LEFT_PAREN:
                match(TokenType.LEFT_PAREN);
                answer = expression();
                match( TokenType.RIGHT_PAREN);
                break;
            case INTEGER:
                //num();
                String numberString = lookahead.getLexeme();
                answer = new ValueNode(numberString);
                match( TokenType.INTEGER);
                break;
            case NOT:
                match( TokenType.NOT);
                OperationNode on = new OperationNode(TokenType.NOT);
                on.setRight(factor());
                return on;
            case ID:
                String idString = lookahead.getLexeme();
                answer = new VariableNode(idString);
                match( TokenType.ID);
                //todo implement array
//                if(lookahead.getType() == TokenType.LEFT_BRACKET){
//                    match(TokenType.LEFT_BRACKET);
//                    expression();
//                    match(TokenType.RIGHT_BRACKET);
//                }
                
                if(lookahead.getType() == TokenType.LEFT_PAREN){
                    match(TokenType.LEFT_PAREN);
                    //answer = expression_list();
                    match(TokenType.RIGHT_PAREN);
                }
                
                else{
                    //lambda
                }
                break;
            default:
                error("Factor");
                break;
        }
        return answer;
    }
    
    public void saveIndentedToString(String filename, ProgramNode pn) throws IOException{
        File syntaxTreeFile = new File("../../SyntaxTrees/" + filename);
        BufferedWriter out = new BufferedWriter(new FileWriter(syntaxTreeFile)); 
        out.write(pn.indentedToString(0));
        out.close();
    }
    
    /**
     * Matches the expected token.
     * If the current token in the input stream from the Scanner
 matches the token that is expected, the current token is
 consumed and the Scanner will move on to the next token
 in the input.
 The null at the end of the file returned by the
 Scanner is replaced with a fake token containing no
 type.
     * @param expected The expected token type.
     */
    public void match( TokenType expected) {
        //System.out.println(expected);
        if( this.lookahead.getType() == expected) {
            try {
                this.lookahead = scanner.nextToken();
                if( this.lookahead == null) {
                    this.lookahead = new Token( "End of File", null);
                }
            } catch (IOException ex) {
                error( "Scanner exception");
            }
        }
        else {
            error("Match of " + expected + " found " + this.lookahead.getType()
                    + " instead.");
            return;
        }
    }
    
    /**
     * Errors out of the parser.
     * Prints an error message and then exits the program.
     * @param message The error message to print.
     */
    public void error( String message) {
        System.out.println( "Error: " + message);
        System.exit( 1);
    }
    
    public void printKeys(){
        table.showKeys();
    }
    
}
