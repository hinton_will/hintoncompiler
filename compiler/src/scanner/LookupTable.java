package scanner;

import java.util.HashMap;

public class LookupTable extends HashMap<String,TokenType> {

    public LookupTable() {
        this.put( "and", TokenType.AND);
        this.put( "array", TokenType.ARRAY);
        this.put( "begin", TokenType.BEGIN);
        this.put( "div", TokenType.DIV);
        this.put( "do", TokenType.DO);
        this.put( "else", TokenType.ELSE);
        this.put( "end", TokenType.END);
        this.put( "function", TokenType.FUNCTION);
        this.put( "if", TokenType.IF);
        this.put("integer", TokenType.INTEGER);
        this.put("num", TokenType.NUM);
        this.put( "mod", TokenType.MOD);
        this.put( "not", TokenType.NOT);
        this.put( "of", TokenType.OF);
        this.put( "or", TokenType.OR);
        this.put( "procedure", TokenType.PROCEDURE);
        this.put( "program", TokenType.PROGRAM);
        this.put( "real", TokenType.REAL);
        this.put( "then", TokenType.THEN);
        this.put( "while", TokenType.WHILE);
        this.put( "var", TokenType.VAR);
        this.put( "+", TokenType.PLUS);
        this.put( "-", TokenType.MINUS);
        this.put( ";", TokenType.SEMI);
        this.put( ",", TokenType.COMMA);
        this.put( ":=", TokenType.ASSIGN);
        this.put( ":", TokenType.COLON);
        this.put( "*", TokenType.MULTIPLY);
        this.put( "/", TokenType.DIVIDE);
        this.put( "(", TokenType.LEFT_PAREN);
        this.put( ")", TokenType.RIGHT_PAREN);
        this.put( "[", TokenType.LEFT_BRACKET);
        this.put( "]", TokenType.RIGHT_BRACKET);
        this.put( ".", TokenType.DOT);
        this.put( "=", TokenType.EQUAL);
        this.put( "<=", TokenType.LTE);
        this.put( ">=", TokenType.GTE);
        this.put( "<", TokenType.LESS);
        this.put( ">", TokenType.GREATER);
        this.put( "<>", TokenType.NOT_EQUAL);
        this.put( "E", TokenType.EXPONENT);
        this.put( "read", TokenType.READ);
        this.put( "write", TokenType.WRITE);
        this.put( "", TokenType.ID);
    } 
}
