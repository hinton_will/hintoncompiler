
package syntaxtree;

import java.util.ArrayList;

/**
 * Represents a compound statement in Pascal.
 * A compound statement is a block of zero or more
 * statements to be run sequentially.
 * @author ErikSteinmetz
 */
public class CompoundStatementNode extends StatementNode {
    
    private ArrayList<StatementNode> statements = new ArrayList<StatementNode>();
    
    public void addStatement( StatementNode state) {
        this.statements.add( state);
    }
    
    public ArrayList<StatementNode> getStatements(){
        ArrayList<StatementNode> statementList = new ArrayList<StatementNode>();
        
        for(StatementNode statement : statements){
            statementList.add(statement);
            if(statement instanceof CompoundStatementNode){
                //statementList.addAll(((CompoundStatementNode)statement).getStatements());
            }
        }
        
        return statementList;
    }
    
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Compound Statement\n";
        for( StatementNode state : statements) {
            answer += state.indentedToString( level + 1);
        }
        return answer;
    }
}
