
package syntaxtree;

import java.util.ArrayList;

/**
 * Represents a set of declarations in a Pascal program.
 * @author Erik Steinmetz
 * @author Will Hinton
 */
public class DeclarationsNode extends SyntaxTreeNode {
    
    private ArrayList<VariableNode> vars = new ArrayList<VariableNode>();
    
    public void addVariable( VariableNode aVariable) {
        vars.add( aVariable);
    }
    
    public ArrayList<String> getVariableNames(){
        ArrayList<String> variableNames = new ArrayList<String>();
        for(VariableNode var : vars){
            variableNames.add(var.getName());
        }
        
        return variableNames;
    }
    
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Declarations:\n";
        for( VariableNode variable : vars) {
            answer += variable.indentedToString( level + 1);
        }
        return answer;
    }
}
