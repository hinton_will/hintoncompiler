/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import scanner.TokenType;
import syntaxtree.DeclarationsNode;
import syntaxtree.ExpressionNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import syntaxtree.WhileStatementNode;

/**
 *
 * @author Will
 */
public class ParserTest {
    
    public ParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of program method, of class Parser.
     */
    @Test
    public void testProgram() throws IOException {
        System.out.println("program");
        Parser instance = new Parser("{ sample.pas }\n" +
"{ An example program for a syntax tree.}\n" +
"{ Exchange rates as of 9 March 2017.}\n" +
"\n" +
"program sample;\n" +
"var dollars, yen, bitcoins: integer;\n" +
"\n" +
"begin\n" +
"  dollars := 1000000;\n" +
"  yen := dollars * 114;\n" +
"  bitcoins := dollars / 1184\n" +
"end\n" +
".", false);
        ProgramNode result = instance.program();
        System.out.println(result.indentedToString(0));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of declarations method, of class Parser.
     */
    @Test
    public void testDeclarations() {
        System.out.println("declarations");
        Parser instance = new Parser("var myBank, myMoney, dollarsOrYen: real;", false);
        DeclarationsNode result = instance.declarations();
        System.out.println(result.indentedToString(0));
        // TODO review the generated test code and remove the default call to fail.
    }

    
    @Test
    public void testExpression(){
        System.out.println("expression");
        Parser instance = new Parser("5 = 4 + 2", false);
        ExpressionNode result = instance.expression();
        System.out.println(result.indentedToString(0));
    }
    /**
     * Test of simple_expression method, of class Parser.
     */
    @Test
    public void testSimple_expression() {
        System.out.println("simple_expression");
        Parser instance = new Parser("5 + 3 + 5 + 5", false);
        ExpressionNode result = instance.simple_expression();
        String expResult = "Operation: PLUS\n|-- Operation: PLUS\n|-- --- Operation: PLUS\n|-- --- --- Value: 5\n|-- --- --- Value: 3\n|-- --- Value: 5\n|-- Value: 5\n";
        System.out.println(result.indentedToString(0));
        assertEquals(expResult, result.indentedToString(0));
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }



//    /**
//     * Test of statement method, of class Parser.
//     */
//    @Test
//    public void testStatement() {
//        System.out.println("statement");
//        Parser instance = new Parser("if 6 > 5 then ", false);
//        StatementNode result = instance.statement();
//        System.out.println(result.indentedToString(0));
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of factor method, of class Parser.
     */
    @Test
    public void testFactor() {
        System.out.println("factor");
        Parser instance = new Parser("fubar", false);
        ExpressionNode expResult = new VariableNode("fubar");
        ExpressionNode result = instance.factor();
        assertEquals(expResult.indentedToString(0), result.indentedToString(0));
        
        
        instance = new Parser("73", false);
        expResult = new ValueNode("73");
        result = instance.factor();
        assertEquals(expResult.indentedToString(0), result.indentedToString(0));
        
    }
 
}
