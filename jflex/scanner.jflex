/* declarations */
package Scanner;
%%

%public
%class scanner		/* names the class MyScanner */
%function nextToken		/* renames yylex() as nextToken() */
%type Token
%line                       // turn on line counting
%column                     // turn on column counting

%eofval{                    // return on end of file
  return null;
%eofval}

%{
private LookupTable table = new LookupTable();
public int getLine() { return yyline;}
public int getColumn() { return yycolumn;}
%}
/* patterns */

digit		= [0-9]
letter		= [A-Za-z]
word		= {letter}({letter}|{digit})*
/*lambda		= ;*/

digits		= {digit}{digit}*
symbolChar	= [;,\,,.,:,[,],(,),+,-,=,<,<,<,>,>,*,/,:]
symbol		= {symbolChar}{symbolChar}*
whitespace	= [\ \n\r\t\f]
comment		= "{" {commentcontent} "}"
commentcontent = ( [^}] )*


%%

/* lexical rules */

{digits}		    {
				Token t = new Token( yytext(), TokenType.INTEGER);
				return t;
				}
				
{whitespace}    { 
                 /* ignore whitespace */ 
                }
				

{comment}		{
				/* ignore comments */
				}
				
{symbol}        {
                 String lexeme = yytext();
				 TokenType tt = table.get(lexeme);
				 if(tt == null){
					System.out.println("Error: unrecognized symbol " + yytext() + " found on line " + getLine());
					System.exit(1);
					}
				 Token t = new Token(yytext(), tt);
				 return t;
				 }
				 
{word}		{
				String lexeme = yytext();
				TokenType tt = table.get(lexeme);
				if(tt == null)
					tt = TokenType.ID;
				Token t = new Token(yytext(), tt);
				return t;
				}
				 
				 
.				{
				 System.out.println("Error: Invalid symbol. " + yytext());
				 
				}


